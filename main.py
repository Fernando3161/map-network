'''
Created on 30.04.2021

@author: Fernando Penaherrera (OFFIS/Uni Oldenburg)
'''
import base64
from os.path import isfile, join
from os import listdir
import pandas as pd
import plotly.graph_objects as go
import networkx as nx
import numpy as np
import os

# Get countries and capital coordinates
country_data = pd.read_csv("./data/country-capitals.csv")
country_data = country_data[country_data["CapitalName"] != "NaN"]
country_data["name_lower"] = country_data["CountryName"].apply(str.lower)
country_data["y"] = country_data["CapitalLatitude"]  
country_data["x"] = country_data["CapitalLongitude"]  

country_borders = pd.read_csv("./data/country-borders.csv")

country_area = pd.read_csv("./data/country-area.csv")
country_area["Area"] = [int(x) for x in country_area["Area"]]
country_area["area_n"] = country_area["Area"] / \
    max(country_area["Area"].values)
country_area.set_index("Code", inplace=True)

"""
data_structure= {
    "AR":{
        name: "Argentina"
        capital:[x,y],
        borders: [CI,BR]
        }
"""
data = {}
for c in country_data["CountryCode"]:
    country = country_data[country_data["CountryCode"] == c]
    name = country["CountryName"].values
    capital_x = country["x"].values
    capital_y = country["y"].values
    c_borders = country_borders[country_borders["country_code"]
                                == c]["country_border_code"].values
    if len(name) > 0 and len(capital_x) > 0:
        data[c] = {
            "name": name[0],
            "capital": [capital_x[0], capital_y[0]],
            "borders": [b for b in c_borders if b not in ["nan"]],
            "area": 0
        }
        if c in country_area.index:
            data[c]["area"] = country_area.loc[c]["area_n"]

df = pd.DataFrame({"from": [c for c in country_borders["country_code"]],
                   "to": [c for c in country_borders["country_border_code"]]
                   })
df = df.replace(np.nan, '', regex=True)

# Build your graph
G = nx.from_pandas_edgelist(df, "from", "to")

edge_x = []
edge_y = []
for edge in G.edges():
    if (edge[1] in country_data["CountryCode"].values) and (
            edge[0] in country_data["CountryCode"].values):
        x0, y0 = data[edge[0]]["capital"]
        x1, y1 = data[edge[1]]["capital"]
        edge_x.append(x0)
        edge_x.append(x1)
        edge_x.append(None)
        edge_y.append(y0)
        edge_y.append(y1)
        edge_y.append(None)


edge_trace = go.Scattergeo(
    lon=edge_x, lat=edge_y,
    line=dict(width=0.5, color='#888'),
    hoverinfo='all',
    mode='lines')
# Plot it

node_x = []
node_y = []
for d in data.keys():
    x, y = data[d]["capital"]
    node_x.append(x)
    node_y.append(y)

# Optional, if markers are wanted 
# node_trace = go.Scattergeo(
#     lon=node_x, lat=node_y,
#     mode='markers',
#     marker_color="black",
#     hoverinfo='text')

fig = go.Figure(data=[edge_trace])  # , node_trace])

fig.update_layout(

    geo=dict(
        showframe=True,
        showcoastlines=False,
        projection_type='equirectangular'
    )
)
"""
['equirectangular', 'mercator', 'orthographic', 'natural
earth', 'kavrayskiy7', 'miller', 'robinson', 'eckert4',
'azimuthal equal area', 'azimuthal equidistant', 'conic
equal area', 'conic conformal', 'conic equidistant',
'gnomonic', 'stereographic', 'mollweide', 'hammer',
'transverse mercator', 'albers usa', 'winkel tripel',
'aitoff', 'sinusoidal']
"""

fig.update_layout(
    autosize=True,
    width=2000,
    height=1000,
    font=dict(
        family="Courier New, monospace",
        size=20, color="black"),
    showlegend=False
)



mypath = os.getcwd() + "/data/country-flags/png/"
files = [f for f in listdir(mypath) if isfile(join(mypath, f))]


for d in data.keys():
    name = data[d]["name"].lower()
    is_in_folder = False
    for f in files:
        if name in f.lower():
            is_in_folder = True
            data[d]["flag"] = f

x_min = min([data[d]["capital"][0]
            for d in data.keys() if "flag" in data[d].keys()])
x_max = max([data[d]["capital"][0]
            for d in data.keys() if "flag" in data[d].keys()])
y_min = min([data[d]["capital"][1]
            for d in data.keys() if "flag" in data[d].keys()])
y_max = max([data[d]["capital"][1]
            for d in data.keys() if "flag" in data[d].keys()])
x_abs_0 = min([abs(data[d]["capital"][0])
              for d in data.keys() if "flag" in data[d].keys()])
extremes = []
for d in data.keys():
    if data[d]["capital"][0] in [x_min, x_max, x_abs_0, -
                                 x_abs_0] or data[d]["capital"][1] in [y_min, y_max]:
        extremes.append(d)


i = 0
# need country size data

fig.update_layout(
    autosize=True,
    width=2000,
    height=1000,
    font=dict(
        family="Courier New, monospace",
        size=20, color="black"),
    showlegend=False
)

for d in data.keys():
    if "flag" in data[d].keys():  # and d in extremes:
        source = "D:/Programming/map-network/data/country-flags/png/" + \
            data[d]["flag"]
        x, y = data[d]["capital"]
        width = (data[d]["area"])**(1 / 5) / 20
        if width < 0.01:
            width = 0.01
        with open(source, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read()).decode()

            encoded_image = "data:image/png;base64," + encoded_string

        fig.add_layout_image(
            dict(
                source=encoded_image,
                xref="paper",
                yref="paper",
                x=(x / 1.12 + 180) / 360 - width / 2 / 2 + 1 / 1000,
                y=(y + 90) / 180 + width / 2,
                sizex=width,
                sizey=width,
                sizing="contain",
                opacity=1,
                layer="above")
        )
        i += 1

fig.update_layout(
    title_text="Country Borders",
    title_x=0.5,
)

fig.update_layout(
    annotations=[
        dict(
            text='@Fernando3161<br>u/Fernando3161<br>Data:wikipedia.org',
            align='left',
            showarrow=False,
            xref='paper',
            yref='paper',
            x=0.0542,
            y=1,
            bordercolor="black",
            borderwidth=1,
        )
    ]
)


fig.write_image("fig1.png")
fig.show()

if __name__ == '__main__':
    pass
